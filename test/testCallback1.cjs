let boardInfo = require("../callback1.cjs");
const path = require('path');

let board = path.resolve('../boards.json');
let boardId = "abc122dc";

boardInfo(board, boardId, (error, data) => {
    if(error){
        console.log("entering error");
        console.log(error);
    }
    else{
        console.log(data);
    }
})