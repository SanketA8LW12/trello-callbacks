let fs = require('fs');


const boardInfo = require('./callback1.cjs');
const boardList = require('./callback2.cjs');
const allCards = require('./callback3.cjs');

function getInfoFromPreviousFunctionThree(boards, cards, lists, Thanos) {

    setTimeout(() => {

        boardInfo(boards, Thanos, (error, data) => {
            if (error) {
                console.log(error);
            }
            else {
                console.log(data);
            }

            boardList(lists, Thanos, (error, data) => {
                if (error) {
                    console.log(error);
                } else {
                    console.log(data);

                    data.map((findName) => {

                        allCards(cards, findName.id, (error, data) => {
                            if (error) {
                                console.log(error);
                            } else {
                                if (data != undefined) {
                                    console.log(data);
                                }
                            }
                        })
                    })
                }
            })
        })

    }, 2000)

}
module.exports = getInfoFromPreviousFunctionThree;
