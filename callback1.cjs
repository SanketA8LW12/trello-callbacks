const fs = require('fs');
const path = require('path');

function boardInfo(board, boardId, callback){
    setTimeout(() => {
        fs.readFile(board, 'utf-8', (error, data)=>{
            if(error){
                console.log(error);
            }
            else{
                let boardFile = JSON.parse(data);
                let boardData = boardFile.filter((data) =>{
                    return data.id === boardId;
                });
                callback(null, boardData);
            }
        })
    }, 2 * 1000)
}

module.exports = boardInfo;