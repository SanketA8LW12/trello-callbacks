const fs = require('fs');
const path = require('path');

function boardList(list, boardId, callback){
    setTimeout(()=>{
        fs.readFile(list, 'utf-8', (error, data) => {
            if(error){
                console.log(error);
            }
            else{
                let boardFile = JSON.parse(data);
                //console.log(boardFile);
                callback(null, boardFile[boardId]);
            }
        })
    }, 2 * 1000)
}

module.exports = boardList;