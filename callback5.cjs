let fs = require('fs');


const boardInfo = require('./callback1.cjs');
const boardList = require('./callback2.cjs');
const allCards = require('./callback3.cjs');

function getInfoFromPreviousFunctionTwo(boards, cards,lists,Thanos){

   setTimeout(()=>{

     boardInfo(boards,Thanos,(error, data)=>{
        if(error)
        {
            console.log(error);
        }
        else{     
            console.log(data);   

            boardList(lists, Thanos, (error, data)=>{
            if(error){
                console.log(error);
            }
            else{
                console.log(data);
                
                const listOfId = data.filter((findName)=>{
                return findName.name === 'Mind' || findName.name === 'Space';
              })
                allCards(cards, listOfId[0].id, (error, data)=>{
                    if(error){
                        console.log(error);
                    }else{
                        console.log(data);
                    }
                })

                allCards(cards, listOfId[1].id, (error, data)=>{
                    if(error){
                        console.log(error);
                    }
                    else{
                        console.log(data);
                    }
                })
            }
        })}
    })

  },2 * 1000)

}
module.exports= getInfoFromPreviousFunctionTwo;
