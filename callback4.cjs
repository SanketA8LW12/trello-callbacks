const boardInfo = require('./callback1.cjs');
const boardList = require('./callback2.cjs');
const allCards = require('./callback3.cjs');

function getInfoFromPreviousFunction(board, cards, list, Thanos){
    setTimeout(()=>{
        boardInfo(board, Thanos, (error, data)=>{
            if(error){
                console.log(error);
            }
            else{
                console.log(data);

                boardList(list, Thanos, (error, data)=>{
                    if(error){
                        console.log(error);
                    }
                    else{
                        console.log(data);
                        const listOfId = data.filter((findName)=>{
                            return findName.name === 'Mind';
                          })
            

                        allCards(cards, listOfId[0].id, (error, data)=>{
                            if(error){
                                console.log(error);
                            }
                            else{
                                console.log(data);
                            }
                        })
                    }
                })
            }
        })
    }, 2 * 1000)
}

module.exports = getInfoFromPreviousFunction;