const fs = require('fs');
const path = require('path');

function allCards(cards, listId, callback){
    setTimeout(()=>{
        fs.readFile(cards, 'utf-8', (error, data)=>{
            if(error){
                console.log(error);
            }
            else{
                let cardFile = JSON.parse(data);
                //console.log(cardFile);
                callback(null, cardFile[listId]);
            }
        })
    }, 2 * 1000)
}


module.exports = allCards;